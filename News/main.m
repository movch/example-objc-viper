//
//  main.m
//  News
//
//  Created by Michael Ovchinnikov on 29/11/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
