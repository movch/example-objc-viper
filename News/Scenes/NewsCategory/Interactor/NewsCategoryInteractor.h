//
//  NewsCategoryInteractor.h
//  News
//
//  Created by Michail Ovchinnikov on 04/12/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsCategoryInteractorInput.h"

@protocol NewsCategoryInteractorOutput;

@interface NewsCategoryInteractor : NSObject <NewsCategoryInteractorInput>

@property (nonatomic, weak) id<NewsCategoryInteractorOutput> output;

@end
