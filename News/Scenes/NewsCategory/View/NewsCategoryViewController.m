//
//  NewsCategoryViewController.m
//  News
//
//  Created by Michail Ovchinnikov on 04/12/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsCategoryViewController.h"

#import "NewsCategoryViewOutput.h"

@implementation NewsCategoryViewController

#pragma mark - IB bindings

- (IBAction)sportNewsButtonClick:(id)sender {
    [self.output showSportNewsFromViewController:self];
}

- (IBAction)cultureNewsButtonClick:(id)sender {
    [self.output showCultureNewsFromViewController:self];
}

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    
	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы NewsCategoryViewInput

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
}

@end
