//
//  NewsCategoryViewController.h
//  News
//
//  Created by Michail Ovchinnikov on 04/12/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NewsCategoryViewInput.h"

@protocol NewsCategoryViewOutput;

@interface NewsCategoryViewController : UIViewController <NewsCategoryViewInput>

@property (nonatomic, strong) id<NewsCategoryViewOutput> output;

@end
