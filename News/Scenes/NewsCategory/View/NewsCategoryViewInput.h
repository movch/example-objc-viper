//
//  NewsCategoryViewInput.h
//  News
//
//  Created by Michail Ovchinnikov on 04/12/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NewsCategoryViewInput <NSObject>

/**
 @author Michail Ovchinnikov

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;

@end
