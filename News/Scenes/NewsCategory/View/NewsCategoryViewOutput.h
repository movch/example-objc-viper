//
//  NewsCategoryViewOutput.h
//  News
//
//  Created by Michail Ovchinnikov on 04/12/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NewsCategoryViewOutput <NSObject>

- (void)didTriggerViewReadyEvent;

- (void)showSportNewsFromViewController:(UIViewController *)viewController;
- (void)showCultureNewsFromViewController:(UIViewController *)viewController;

@end
