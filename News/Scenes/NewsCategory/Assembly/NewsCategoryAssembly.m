//
//  NewsCategoryAssembly.m
//  News
//
//  Created by Michail Ovchinnikov on 04/12/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsCategoryAssembly.h"

#import "NewsCategoryViewController.h"
#import "NewsCategoryInteractor.h"
#import "NewsCategoryPresenter.h"
#import "NewsCategoryRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation NewsCategoryAssembly

- (NewsCategoryViewController *)viewNewsCategory {
    return [TyphoonDefinition withClass:[NewsCategoryViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterNewsCategory]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterNewsCategory]];
                          }];
}

- (NewsCategoryInteractor *)interactorNewsCategory {
    return [TyphoonDefinition withClass:[NewsCategoryInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterNewsCategory]];
                          }];
}

- (NewsCategoryPresenter *)presenterNewsCategory{
    return [TyphoonDefinition withClass:[NewsCategoryPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewNewsCategory]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorNewsCategory]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerNewsCategory]];
                          }];
}

- (NewsCategoryRouter *)routerNewsCategory{
    return [TyphoonDefinition withClass:[NewsCategoryRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewNewsCategory]];
                          }];
}

@end
