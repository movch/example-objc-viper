//
//  NewsCategoryPresenter.m
//  News
//
//  Created by Michail Ovchinnikov on 04/12/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsCategoryPresenter.h"

#import "NewsCategoryViewInput.h"
#import "NewsCategoryInteractorInput.h"
#import "NewsCategoryRouterInput.h"

@implementation NewsCategoryPresenter

#pragma mark - Методы NewsCategoryModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы NewsCategoryViewOutput

- (void)showSportNewsFromViewController:(UIViewController *)viewController {
    [self.router showSportNewsFromViewController:viewController];
}

- (void)showCultureNewsFromViewController:(UIViewController *)viewController {
    [self.router showCultureNewsFromViewController:viewController];
}

- (void)didTriggerViewReadyEvent {
    [self.view setupInitialState];
}


#pragma mark - Методы NewsCategoryInteractorOutput

@end
