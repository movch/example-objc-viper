//
//  NewsCategoryPresenter.h
//  News
//
//  Created by Michail Ovchinnikov on 04/12/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsCategoryViewOutput.h"
#import "NewsCategoryInteractorOutput.h"
#import "NewsCategoryModuleInput.h"

@protocol NewsCategoryViewInput;
@protocol NewsCategoryInteractorInput;
@protocol NewsCategoryRouterInput;

@interface NewsCategoryPresenter : NSObject <NewsCategoryModuleInput, NewsCategoryViewOutput, NewsCategoryInteractorOutput>

@property (nonatomic, weak) id<NewsCategoryViewInput> view;
@property (nonatomic, strong) id<NewsCategoryInteractorInput> interactor;
@property (nonatomic, strong) id<NewsCategoryRouterInput> router;

@end
