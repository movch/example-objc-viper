//
//  NewsCategoryRouter.h
//  News
//
//  Created by Michail Ovchinnikov on 04/12/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsCategoryRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface NewsCategoryRouter : NSObject <NewsCategoryRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
