//
//  NewsCategoryRouter.m
//  News
//
//  Created by Michail Ovchinnikov on 04/12/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsCategoryRouter.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation NewsCategoryRouter

#pragma mark - Методы NewsCategoryRouterInput

-(void) showSportNewsFromViewController:(UIViewController *)viewController {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"sportNewsList"];
    [viewController.navigationController pushViewController:vc animated:YES];
}

-(void) showCultureNewsFromViewController:(UIViewController *)viewController {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"cultureNewsList"];
    [viewController.navigationController pushViewController:vc animated:YES];
}

@end
