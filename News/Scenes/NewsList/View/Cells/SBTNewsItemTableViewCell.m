//
//  SBTNewsItemsTableViewCell.m
//  News
//
//  Created by Michail Ovchinnikov on 07.12.17.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import "SBTNewsItemTableViewCell.h"

@implementation SBTNewsItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
