//
//  CellDecorator.h
//  News
//
//  Created by Michail Ovchinnikov on 05.12.17.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SBTItemProtocol;
@class SBTNewsItemTableViewCell;

@protocol SBTNewsCellDecoratorProtocol <NSObject>

-(void)decorate:(SBTNewsItemTableViewCell *)cell with:(id<SBTItemProtocol>)article;

@end
