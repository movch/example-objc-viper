//
//  SBTNewsDataSourceProtocol.h
//  News
//
//  Created by Michail Ovchinnikov on 06.12.17.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SBTItemProtocol;

@protocol SBTNewsDataSourceProtocol <NSObject>

- (id<SBTItemProtocol>)articleAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)articlesCount;

@end
