//
//  NewsListViewOutput.h
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NewsListViewOutput <NSObject>

- (void)didTriggerViewReadyEvent;
- (void) showArticleAtIndexPath:(NSIndexPath *)indexPath;
    
@end
