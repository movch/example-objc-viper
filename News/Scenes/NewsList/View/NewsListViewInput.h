//
//  NewsListViewInput.h
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NewsListViewInput <NSObject>

- (void)setupInitialState;
- (void)reloadData;
    
@end
