//
//  NewsListViewController.m
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsListViewController.h"
#import "NewsListViewOutput.h"

#import "SBTNewsCellDecoratorProtocol.h"
#import "SBTNewsDataSourceProtocol.h"
#import "SBTItemProtocol.h"
#import "SBTNewsItemTableViewCell.h"

@implementation NewsListViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    
    self.title = @"Новости";
    
    [self setupTableView];
	[self.output didTriggerViewReadyEvent];
}

- (void)setupTableView {
    [[self tableView] registerNib:[UINib nibWithNibName:@"SBTNewsItemTableViewCell" bundle:nil]
           forCellReuseIdentifier:@"SBTNewsItemTableViewCell"];
}

#pragma mark - Методы NewsListViewInput

- (void)setupInitialState {
}
    
- (void)reloadData {
    [self.tableView reloadData];
}

#pragma mark - Методы TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource articlesCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SBTNewsItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SBTNewsItemTableViewCell"];
    id<SBTItemProtocol> article = [self.dataSource articleAtIndexPath:indexPath];
    [self.cellDecorator decorate:cell with:article];
    return cell;
}

#pragma mark - Методы TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.output showArticleAtIndexPath:indexPath];
}

@end
