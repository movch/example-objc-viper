//
//  SBTNewsListCellDecorator.h
//  News
//
//  Created by Michail Ovchinnikov on 06.12.17.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBTNewsCellDecoratorProtocol.h"

@protocol SBTItemProtocol;
@class SBTNewsItemTableViewCell;

@interface SBTNewsListCellDecorator : NSObject<SBTNewsCellDecoratorProtocol>

-(void)decorate:(SBTNewsItemTableViewCell *)cell with:(id<SBTItemProtocol>)article;

@end
