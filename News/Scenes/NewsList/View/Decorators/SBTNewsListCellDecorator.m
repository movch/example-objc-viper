//
//  SBTNewsListCellDecorator.m
//  News
//
//  Created by Michail Ovchinnikov on 06.12.17.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import "SBTNewsListCellDecorator.h"
#import "SBTItemProtocol.h"

#import "SBTNewsItemTableViewCell.h"

@implementation SBTNewsListCellDecorator

-(void)decorate:(SBTNewsItemTableViewCell *)cell with:(id<SBTItemProtocol>)article {
    cell.titleLabel.text = article.title;
    cell.dateLabel.text = article.date;
    cell.descriptionLabel.text = article.text;
}

@end
