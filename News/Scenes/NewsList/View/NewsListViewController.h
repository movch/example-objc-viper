//
//  NewsListViewController.h
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsListViewInput.h"

@protocol NewsListViewOutput;
@protocol SBTNewsDataSourceProtocol;
@protocol SBTNewsCellDecoratorProtocol;

@interface NewsListViewController : UIViewController <NewsListViewInput>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) id<NewsListViewOutput> output;
@property (nonatomic, strong) id<SBTNewsDataSourceProtocol> dataSource;
@property (nonatomic, strong) id<SBTNewsCellDecoratorProtocol> cellDecorator;

@end
