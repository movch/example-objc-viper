//
//  NewsManager.h
//  News
//
//  Created by Michail Ovchinnikov on 05.12.17.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBTNewsDataSourceProtocol.h"

@protocol SBTItemProtocol;

@interface SBTNewsManager: NSObject<UITableViewDataSource, SBTNewsDataSourceProtocol>

- (id<SBTItemProtocol>)articleAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)articlesCount;
- (void) loadNewsFromFile:(NSString *)filename;

@end
