//
//  NewsManager.m
//  News
//
//  Created by Michail Ovchinnikov on 05.12.17.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import "SBTNewsManager.h"
#import "Article.h"
#import "SBTItemProtocol.h"

@interface SBTNewsManager ()

@property (nonatomic, strong) NSArray<Article *> *articles;

@end

@implementation SBTNewsManager

- (void) loadNewsFromFile:(NSString *)filename {
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    NSMutableArray *articles = [[NSMutableArray alloc] init];
    for (NSDictionary *article in json) {
        Article *parsedArticle = [[Article alloc] init];
        [EKMapper fillObject:parsedArticle fromExternalRepresentation:article withMapping:Article.objectMapping];
        [articles addObject:parsedArticle];
    }
    
    self.articles = articles;
}

- (id<SBTItemProtocol>)articleAtIndexPath:(NSIndexPath *)indexPath {
    return self.articles[indexPath.row];
}

- (NSInteger)articlesCount {
    return [self.articles count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    return cell;
}

@end
