//
//  CultureNewsManager.m
//  News
//
//  Created by Michail Ovchinnikov on 04.12.17.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import "SBTCultureNewsManager.h"

@implementation SBTCultureNewsManager

- (void) loadArticles {
    return [self loadNewsFromFile:@"CultureNews"];
}

@end
