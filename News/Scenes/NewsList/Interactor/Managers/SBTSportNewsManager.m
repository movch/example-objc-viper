//
//  NewsManager.m
//  News
//
//  Created by Michail Ovchinnikov on 04.12.17.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import "SBTSportNewsManager.h"

@implementation SBTSportNewsManager

- (void) loadArticles {
    return [self loadNewsFromFile:@"SportNews"];
}

@end
