//
//  NewsProtoclol.h
//  News
//
//  Created by Michail Ovchinnikov on 04.12.17.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SBTItemProtocol;

@protocol SBTNewsManagerProtocol <NSObject>

- (void)loadArticles;
- (id<SBTItemProtocol>)articleAtIndexPath:(NSIndexPath *)indexPath;

@end
