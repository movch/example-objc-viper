//
//  CultureNewsManager.h
//  News
//
//  Created by Michail Ovchinnikov on 04.12.17.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBTNewsManagerProtocol.h"
#import "SBTNewsManager.h"

@interface SBTCultureNewsManager : SBTNewsManager<SBTNewsManagerProtocol>

- (void)loadArticles;

@end
