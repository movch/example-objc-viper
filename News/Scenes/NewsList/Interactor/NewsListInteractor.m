//
//  NewsListInteractor.m
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsListInteractor.h"
#import "NewsListInteractorOutput.h"
#import "SBTNewsManagerProtocol.h"
#import "Article.h"

@implementation NewsListInteractor

#pragma mark - Методы NewsListInteractorOutput

- (void) loadArticles {
    [self.newsManager loadArticles];
    [self.output loadedArticles];
}
    
#pragma mark - Методы NewsListInteractorInput

- (id<SBTItemProtocol>)articleAtIndexPath:(NSIndexPath *)indexPath {
    return [self.newsManager articleAtIndexPath:indexPath];
}

@end
