//
//  NewsListInteractorInput.h
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SBTNewsManagerProtocol;
@protocol SBTItemProtocol;

@protocol NewsListInteractorInput <NSObject>

- (id<SBTItemProtocol>)articleAtIndexPath:(NSIndexPath *)indexPath;
- (void) loadArticles;

@end
