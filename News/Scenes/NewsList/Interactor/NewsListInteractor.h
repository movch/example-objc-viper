//
//  NewsListInteractor.h
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsListInteractorInput.h"

@protocol NewsListInteractorOutput;
@protocol SBTNewsManagerProtocol;

@interface NewsListInteractor : NSObject <NewsListInteractorInput>

@property (nonatomic, strong) id<SBTNewsManagerProtocol> newsManager;
@property (nonatomic, weak) id<NewsListInteractorOutput> output;

@end
