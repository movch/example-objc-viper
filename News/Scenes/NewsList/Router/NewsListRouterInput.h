//
//  NewsListRouterInput.h
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SBTItemProtocol;

@protocol NewsListRouterInput <NSObject>

- (void) showDetailsFor:(id<SBTItemProtocol>)article;

@end
