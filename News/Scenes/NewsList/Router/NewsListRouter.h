//
//  NewsListRouter.h
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsListRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface NewsListRouter : NSObject <NewsListRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
