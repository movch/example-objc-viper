//
//  NewsListRouter.m
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "NewsListRouter.h"
#import "NewsDetailsModuleInput.h"
#import "SBTItemProtocol.h"

@implementation NewsListRouter

#pragma mark - Методы NewsListRouterInput

- (void) showDetailsFor:(id<SBTItemProtocol>)article {
    [[self.transitionHandler openModuleUsingSegue:@"showArticleDetails"]
     thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<NewsDetailsModuleInput> moduleInput) {
         [moduleInput configureModuleWithArticle: article];
         return nil;
     }];
}

@end
