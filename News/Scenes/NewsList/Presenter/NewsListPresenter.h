//
//  NewsListPresenter.h
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsListViewOutput.h"
#import "NewsListInteractorOutput.h"
#import "NewsListModuleInput.h"

@protocol NewsListViewInput;
@protocol NewsListInteractorInput;
@protocol NewsListRouterInput;

@interface NewsListPresenter : NSObject <NewsListModuleInput, NewsListViewOutput, NewsListInteractorOutput>

@property (nonatomic, weak) id<NewsListViewInput> view;
@property (nonatomic, strong) id<NewsListInteractorInput> interactor;
@property (nonatomic, strong) id<NewsListRouterInput> router;

@end
