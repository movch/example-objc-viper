//
//  NewsListModuleInput.h
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol NewsListModuleInput <RamblerViperModuleInput>

/**
 @author Michail Ovchinnikov

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModule;

@end
