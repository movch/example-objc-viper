//
//  NewsListPresenter.m
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsListPresenter.h"

#import "NewsListViewInput.h"
#import "NewsListInteractorInput.h"
#import "NewsListRouterInput.h"

#import "SBTItemProtocol.h"

@implementation NewsListPresenter

#pragma mark - Методы NewsListModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы NewsListViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
    [self.interactor loadArticles];
}

- (void) showArticleAtIndexPath:(NSIndexPath *)indexPath {
    id<SBTItemProtocol> article = [self.interactor articleAtIndexPath:indexPath];
    [self.router showDetailsFor:article];
}

#pragma mark - Методы NewsListInteractorOutput

- (void)loadedArticles {
    [self.view reloadData];
}

@end
