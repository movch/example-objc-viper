//
//  NewsListAssembly.m
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsListAssembly.h"

#import "NewsListViewController.h"
#import "NewsListInteractor.h"
#import "NewsListPresenter.h"
#import "NewsListRouter.h"

#import "SBTSportNewsManager.h"
#import "SBTCultureNewsManager.h"
#import "SBTNewsListCellDecorator.h"

@implementation NewsListAssembly

- (NewsListViewController *)viewSportNewsList {
    return [TyphoonDefinition withClass:[NewsListViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSportNewsList]];
                              [definition injectProperty:@selector(dataSource)
                                                    with:[self managerSportNewsList]];
                              [definition injectProperty:@selector(cellDecorator)
                                                    with:[SBTNewsListCellDecorator new]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSportNewsList]];
                          }];
}

- (NewsListViewController *)viewCultureNewsList {
    return [TyphoonDefinition withClass:[NewsListViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCultureNewsList]];
                              [definition injectProperty:@selector(dataSource)
                                                    with:[self managerCultureNewsList]];
                              [definition injectProperty:@selector(cellDecorator)
                                                    with:[SBTNewsListCellDecorator new]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterCultureNewsList]];
                          }];
}

- (NewsListInteractor *)interactorSportNewsList {
    return [TyphoonDefinition withClass:[NewsListInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSportNewsList]];
                              [definition injectProperty:@selector(newsManager)
                                                    with:[self managerSportNewsList]];
                          }];
}

- (NewsListInteractor *)interactorCultureNewsList {
    return [TyphoonDefinition withClass:[NewsListInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCultureNewsList]];
                              [definition injectProperty:@selector(newsManager)
                                                    with:[self managerCultureNewsList]];
                          }];
}

- (NewsListInteractor *)managerSportNewsList {
    return [TyphoonDefinition withClass:[SBTSportNewsManager class]];
}

- (NewsListInteractor *)managerCultureNewsList {
    return [TyphoonDefinition withClass:[SBTCultureNewsManager class]];
}

- (NewsListPresenter *)presenterSportNewsList{
    return [TyphoonDefinition withClass:[NewsListPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSportNewsList]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSportNewsList]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSportNewsList]];
                          }];
}

- (NewsListPresenter *)presenterCultureNewsList{
    return [TyphoonDefinition withClass:[NewsListPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCultureNewsList]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCultureNewsList]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCultureNewsList]];
                          }];
}

- (NewsListRouter *)routerSportNewsList{
    return [TyphoonDefinition withClass:[NewsListRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSportNewsList]];
                          }];
}

- (NewsListRouter *)routerCultureNewsList{
    return [TyphoonDefinition withClass:[NewsListRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewCultureNewsList]];
                          }];
}

@end
