//
//  NewsListAssembly.h
//  News
//
//  Created by Michail Ovchinnikov on 29/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Michail Ovchinnikov

 NewsList module
 */
@interface NewsListAssembly : TyphoonAssembly<RamblerInitialAssembly>

@end
