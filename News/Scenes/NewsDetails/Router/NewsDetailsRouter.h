//
//  NewsDetailsRouter.h
//  News
//
//  Created by Michail Ovchinnikov on 30/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsDetailsRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface NewsDetailsRouter : NSObject <NewsDetailsRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
