//
//  NewsDetailsInteractor.h
//  News
//
//  Created by Michail Ovchinnikov on 30/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsDetailsInteractorInput.h"

@protocol NewsDetailsInteractorOutput;

@interface NewsDetailsInteractor : NSObject <NewsDetailsInteractorInput>

@property (nonatomic, weak) id<NewsDetailsInteractorOutput> output;

@end
