//
//  NewsDetailsPresenter.h
//  News
//
//  Created by Michail Ovchinnikov on 30/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsDetailsViewOutput.h"
#import "NewsDetailsInteractorOutput.h"
#import "NewsDetailsModuleInput.h"

@protocol NewsDetailsViewInput;
@protocol NewsDetailsInteractorInput;
@protocol NewsDetailsRouterInput;

@interface NewsDetailsPresenter : NSObject <NewsDetailsModuleInput, NewsDetailsViewOutput, NewsDetailsInteractorOutput>

@property (nonatomic, weak) id<NewsDetailsViewInput> view;
@property (nonatomic, strong) id<NewsDetailsInteractorInput> interactor;
@property (nonatomic, strong) id<NewsDetailsRouterInput> router;

@end
