//
//  NewsDetailsModuleInput.h
//  News
//
//  Created by Michail Ovchinnikov on 30/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol SBTItemProtocol;

@protocol NewsDetailsModuleInput <RamblerViperModuleInput>

- (void)configureModuleWithArticle:(id<SBTItemProtocol>) article;

@end
