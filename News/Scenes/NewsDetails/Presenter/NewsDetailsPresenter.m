//
//  NewsDetailsPresenter.m
//  News
//
//  Created by Michail Ovchinnikov on 30/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsDetailsPresenter.h"

#import "NewsDetailsViewInput.h"
#import "NewsDetailsInteractorInput.h"
#import "NewsDetailsRouterInput.h"

#import "NewsDetailsViewController.h"

#import "SBTItemProtocol.h"

@interface NewsDetailsPresenter ()

@property (nonatomic, strong) id<SBTItemProtocol> article;

@end


@implementation NewsDetailsPresenter

#pragma mark - Методы NewsDetailsModuleInput

- (void)configureModuleWithArticle:(id<SBTItemProtocol>) article {
    self.article = article;
}

#pragma mark - Методы NewsDetailsViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
    [self.view handleArticle: self.article];
}

#pragma mark - Методы NewsDetailsInteractorOutput

@end
