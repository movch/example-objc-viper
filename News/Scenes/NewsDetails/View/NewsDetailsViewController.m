//
//  NewsDetailsViewController.m
//  News
//
//  Created by Michail Ovchinnikov on 30/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsDetailsViewController.h"
#import "NewsDetailsViewOutput.h"
#import "Article.h"

@implementation NewsDetailsViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы NewsDetailsViewInput

- (void)setupInitialState {
}

- (void)handleArticle:(Article *)article {
    self.title = article.title;
    self.titleLabel.text = article.title;
    self.textLabel.text = article.text;
}

@end
