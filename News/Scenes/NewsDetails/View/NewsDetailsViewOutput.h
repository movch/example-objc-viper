//
//  NewsDetailsViewOutput.h
//  News
//
//  Created by Michail Ovchinnikov on 30/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NewsDetailsViewController;

@protocol NewsDetailsViewOutput <NSObject>

- (void)didTriggerViewReadyEvent;

@end
