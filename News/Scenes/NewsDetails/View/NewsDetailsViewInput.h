//
//  NewsDetailsViewInput.h
//  News
//
//  Created by Michail Ovchinnikov on 30/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SBTItemProtocol;

@protocol NewsDetailsViewInput <NSObject>

- (void)setupInitialState;
- (void)handleArticle:(id<SBTItemProtocol>)article;

@end
