//
//  NewsDetailsViewController.h
//  News
//
//  Created by Michail Ovchinnikov on 30/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsDetailsViewInput.h"

@protocol NewsDetailsViewOutput;

@interface NewsDetailsViewController : UIViewController <NewsDetailsViewInput>

@property (nonatomic, strong) id<NewsDetailsViewOutput> output;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIView *childContainerView;

@end
