//
//  NewsDetailsAssembly.m
//  News
//
//  Created by Michail Ovchinnikov on 30/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import "NewsDetailsAssembly.h"

#import "NewsDetailsViewController.h"
#import "NewsDetailsInteractor.h"
#import "NewsDetailsPresenter.h"
#import "NewsDetailsRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation NewsDetailsAssembly

- (NewsDetailsViewController *)viewNewsDetails {
    return [TyphoonDefinition withClass:[NewsDetailsViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterNewsDetails]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterNewsDetails]];
                          }];
}

- (NewsDetailsInteractor *)interactorNewsDetails {
    return [TyphoonDefinition withClass:[NewsDetailsInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterNewsDetails]];
                          }];
}

- (NewsDetailsPresenter *)presenterNewsDetails{
    return [TyphoonDefinition withClass:[NewsDetailsPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewNewsDetails]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorNewsDetails]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerNewsDetails]];
                          }];
}

- (NewsDetailsRouter *)routerNewsDetails{
    return [TyphoonDefinition withClass:[NewsDetailsRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewNewsDetails]];
                          }];
}

@end
