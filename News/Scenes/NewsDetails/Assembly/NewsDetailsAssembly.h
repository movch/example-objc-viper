//
//  NewsDetailsAssembly.h
//  News
//
//  Created by Michail Ovchinnikov on 30/11/2017.
//  Copyright © 2017 SBT. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Michail Ovchinnikov

 NewsDetails module
 */
@interface NewsDetailsAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
