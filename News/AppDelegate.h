//
//  AppDelegate.h
//  News
//
//  Created by Michael Ovchinnikov on 29/11/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

