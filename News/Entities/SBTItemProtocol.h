//
//  SBTItemProtocol.h
//  News
//
//  Created by Michail Ovchinnikov on 06.12.17.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SBTItemProtocol <NSObject>

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *date;

@end
