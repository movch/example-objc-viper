//
//  Article.h
//  News
//
//  Created by Michael Ovchinnikov on 29/11/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import <EasyMapping.h>
#import "SBTItemProtocol.h"

@interface Article: NSObject<EKMappingProtocol, SBTItemProtocol>

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *date;
    
@end
