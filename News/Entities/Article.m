//
//  Article.m
//  News
//
//  Created by Michael Ovchinnikov on 29/11/2017.
//  Copyright © 2017 Michael Ovchinnikov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Article.h"

@implementation Article
    
+(EKObjectMapping *)objectMapping {
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"title", @"text", @"date"]];
    }];
}
    
@end
